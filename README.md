# Sistema de Registro de Ponto

## Informações do Banco de Dados 
  
- Database: registroponto 
- Senha: postgres

## Usuários do sistema

### Admin

- CPF: 111.122.223-33
- SENHA: 123

### Gerente

- CPF: 444.455.556-66
- SENHA: 456

### Operacional

- CPF: 666.677.778-88
- SENHA: 678
 
