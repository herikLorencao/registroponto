package registroponto.utils;

import registroponto.model.domain.Funcionario;

public class Usuario {

    private Funcionario funcionario;
    private int nivelSistema;

    public Usuario() {
    }

    public Usuario(Funcionario funcionario, int nivelSistema) {
        this.funcionario = funcionario;
        this.nivelSistema = nivelSistema;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public int getNivelSistema() {
        return nivelSistema;
    }

    public void setNivelSistema(int nivelSistema) {
        this.nivelSistema = nivelSistema;
    }
}
