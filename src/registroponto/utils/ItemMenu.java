package registroponto.utils;

public class ItemMenu {

    private String nome;
    private String destino;

    public ItemMenu() {
    }

    public ItemMenu(String nome, String destino) {
        this.nome = nome;
        this.destino = destino;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
