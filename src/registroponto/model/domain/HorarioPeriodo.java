package registroponto.model.domain;

public class HorarioPeriodo {

    private Funcionario funcionario;
    private Periodo periodo;
    private double cargaHoraria;

    public HorarioPeriodo() {
    }

    public HorarioPeriodo(Funcionario funcionario, Periodo periodo, double cargaHoraria) {
        this.funcionario = funcionario;
        this.periodo = periodo;
        this.cargaHoraria = cargaHoraria;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public double getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(double cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }
}
