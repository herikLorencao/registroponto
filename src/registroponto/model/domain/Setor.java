package registroponto.model.domain;

import java.util.List;

public class Setor {

    private int codSetor;
    private String nome;
    private List<Cargo> listCargos;

    public Setor() {
    }

    public Setor(int codSetor, String nome) {
        this.codSetor = codSetor;
        this.nome = nome;
    }

    public int getCodSetor() {
        return codSetor;
    }

    public void setCodSetor(int codSetor) {
        this.codSetor = codSetor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Cargo> getListCargos() {
        return listCargos;
    }

    public void setListCargos(List<Cargo> listCargos) {
        this.listCargos = listCargos;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
