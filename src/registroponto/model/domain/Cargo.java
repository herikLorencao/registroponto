package registroponto.model.domain;

import java.util.List;

public class Cargo {

    private int CodCargo;
    private String nome;
    private Setor setor;
    private double valorHoraTrabalho;
    private int nivelSistema;
    private List<Funcionario> listFuncionarios;

    public Cargo() {
    }

    public Cargo(int CodCargo, String nome, Setor setor, double valorHoraTrabalho, int nivelSistema) {
        this.CodCargo = CodCargo;
        this.nome = nome;
        this.setor = setor;
        this.valorHoraTrabalho = valorHoraTrabalho;
        this.nivelSistema = nivelSistema;
    }

    public int getCodCargo() {
        return CodCargo;
    }

    public void setCodCargo(int CodCargo) {
        this.CodCargo = CodCargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }

    public double getValorHoraTrabalho() {
        return valorHoraTrabalho;
    }

    public void setValorHoraTrabalho(double valorHoraTrabalho) {
        this.valorHoraTrabalho = valorHoraTrabalho;
    }

    public int getNivelSistema() {
        return nivelSistema;
    }

    public void setNivelSistema(int nivelSistema) {
        this.nivelSistema = nivelSistema;
    }

    public List<Funcionario> getListFuncionarios() {
        return listFuncionarios;
    }

    public void setListFuncionarios(List<Funcionario> listFuncionarios) {
        this.listFuncionarios = listFuncionarios;
    }

    @Override
    public String toString() {
        return this.nome;
    }

}
