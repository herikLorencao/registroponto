package registroponto.model.domain;

import java.time.LocalTime;

public class Ponto {

    private int codPonto;
    private Periodo periodo;
    private Funcionario funcionario;
    private LocalTime horaInicio;
    private LocalTime horaFim;

    public Ponto() {
    }

    public Ponto(int codPonto, Periodo periodo, Funcionario funcionario, LocalTime horaInicio, LocalTime horaFim) {
        this.codPonto = codPonto;
        this.periodo = periodo;
        this.funcionario = funcionario;
        this.horaInicio = horaInicio;
        this.horaFim = horaFim;
    }

    public int getCodPonto() {
        return codPonto;
    }

    public void setCodPonto(int codPonto) {
        this.codPonto = codPonto;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public LocalTime getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(LocalTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public LocalTime getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(LocalTime horaFim) {
        this.horaFim = horaFim;
    }
}
