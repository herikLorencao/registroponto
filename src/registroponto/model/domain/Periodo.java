package registroponto.model.domain;

import java.util.List;

public class Periodo {

    private int codMes;
    private String codAno;
    private String label;
    private List<Ponto> listPontos;
    private List<HorarioPeriodo> listHorarioPeriodos;

    public Periodo() {
    }

    public Periodo(int codMes, String codAno, String label) {
        this.codMes = codMes;
        this.codAno = codAno;
        this.label = label;
    }

    public int getCodMes() {
        return codMes;
    }

    public void setCodMes(int codMes) {
        this.codMes = codMes;
    }

    public String getCodAno() {
        return codAno;
    }

    public void setCodAno(String codAno) {
        this.codAno = codAno;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Ponto> getListPontos() {
        return listPontos;
    }

    public void setListPontos(List<Ponto> listPontos) {
        this.listPontos = listPontos;
    }

    public List<HorarioPeriodo> getListHorarioPeriodos() {
        return listHorarioPeriodos;
    }

    public void setListHorarioPeriodos(List<HorarioPeriodo> listHorarioPeriodos) {
        this.listHorarioPeriodos = listHorarioPeriodos;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
