package registroponto.model.domain;

import java.util.List;

public class Funcionario {

    private int codFuncionario;
    private Cargo cargo;
    private String nome;
    private String cpf;
    private String senha;
    private List<Ponto> listPontos;
    private List<HorarioPeriodo> listHorarioPeriodos;

    public Funcionario() {
    }

    public Funcionario(int codFuncionario, Cargo cargo, String nome, String cpf, String senha) {
        this.codFuncionario = codFuncionario;
        this.cargo = cargo;
        this.nome = nome;
        this.cpf = cpf;
        this.senha = senha;
    }

    public int getCodFuncionario() {
        return codFuncionario;
    }

    public void setCodFuncionario(int codFuncionario) {
        this.codFuncionario = codFuncionario;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Ponto> getListPontos() {
        return listPontos;
    }

    public void setListPontos(List<Ponto> listPontos) {
        this.listPontos = listPontos;
    }

    public List<HorarioPeriodo> getListHorarioPeriodos() {
        return listHorarioPeriodos;
    }

    public void setListHorarioPeriodos(List<HorarioPeriodo> listHorarioPeriodos) {
        this.listHorarioPeriodos = listHorarioPeriodos;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
