package registroponto.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import registroponto.model.domain.Periodo;

public class PeriodoDAO {

    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean inserir(Periodo periodo) {
        String sql = "INSERT INTO periodo(cod_mes, cod_ano, label) VALUES(?, ?, ?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, periodo.getCodMes());
            stmt.setString(2, periodo.getCodAno());
            stmt.setString(3, periodo.getLabel());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean alterar(Periodo periodo) {
        String sql = "UPDATE periodo SET label=?"
                + " WHERE cod_mes=? AND cod_ano=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, periodo.getLabel());
            stmt.setInt(2, periodo.getCodMes());
            stmt.setString(3, periodo.getCodAno());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean remover(Periodo periodo) {
        String sql = "DELETE FROM periodo WHERE cod_mes=? AND cod_ano=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, periodo.getCodMes());
            stmt.setString(2, periodo.getCodAno());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public List<Periodo> listar() {
        String sql = "SELECT * FROM periodo";
        List<Periodo> retorno = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Periodo periodo = new Periodo();
                periodo.setCodMes(resultado.getInt("cod_mes"));
                periodo.setCodAno(resultado.getString("cod_ano"));
                periodo.setLabel(resultado.getString("label"));

                retorno.add(periodo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public Periodo buscar(Periodo periodo) {
        String sql = "SELECT * FROM periodo WHERE cod_mes=? AND cod_ano=?";
        Periodo retorno = new Periodo();
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, periodo.getCodMes());
            stmt.setString(2, periodo.getCodAno());
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                periodo.setLabel(resultado.getString("label"));
                retorno = periodo;
            } else {
                retorno = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
}
