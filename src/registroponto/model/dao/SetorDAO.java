package registroponto.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import registroponto.model.domain.Cargo;
import registroponto.model.domain.Setor;

public class SetorDAO {

    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean inserir(Setor setor) {
        String sql = "INSERT INTO setor(nome) VALUES(?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, setor.getNome());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SetorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean alterar(Setor setor) {
        String sql = "UPDATE setor SET nome=? WHERE cod_setor=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, setor.getNome());
            stmt.setInt(2, setor.getCodSetor());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SetorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean remover(Setor setor) {
        CargoDAO cargoDAO = new CargoDAO();
        List<Cargo> listCargos;

        cargoDAO.setConnection(connection);

        String sql = "DELETE FROM setor WHERE cod_setor=?";

        try {
            connection.setAutoCommit(false);
            listCargos = cargoDAO.buscarPorSetor(setor);

            for (Cargo cargo : listCargos) {
                cargoDAO.remover(cargo);
            }

            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, setor.getCodSetor());
            stmt.execute();

            connection.commit();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SetorDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public List<Setor> listar() {
        String sql = "SELECT * FROM setor ORDER BY nome;";
        List<Setor> retorno = new ArrayList<>();
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Setor setor = new Setor();
                setor.setCodSetor(resultado.getInt("cod_setor"));
                setor.setNome(resultado.getString("nome"));
                retorno.add(setor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SetorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public Setor buscar(Setor setor) {
        String sql = "SELECT * FROM setor WHERE cod_setor=?";
        Setor retorno = new Setor();
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, setor.getCodSetor());
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                setor.setNome(resultado.getString("nome"));
                retorno = setor;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SetorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
}
