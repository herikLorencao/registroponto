package registroponto.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.HorarioPeriodo;
import registroponto.model.domain.Periodo;
import registroponto.model.domain.Setor;

public class HorarioPeriodoDAO {

    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean inserir(HorarioPeriodo horarioPeriodo) {
        String sql = "INSERT INTO horario_periodo(funcionario, ano, mes, carga_horaria) "
                + "VALUES(?, ?, ?, ?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, horarioPeriodo.getFuncionario().getCodFuncionario());
            stmt.setString(2, horarioPeriodo.getPeriodo().getCodAno());
            stmt.setInt(3, horarioPeriodo.getPeriodo().getCodMes());
            stmt.setDouble(4, horarioPeriodo.getCargaHoraria());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(HorarioPeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean alterar(HorarioPeriodo horarioPeriodo) {
        String sql = "UPDATE horario_periodo SET carga_horaria=? "
                + "WHERE funcionario=? AND ano=? AND mes=?;";

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setDouble(1, horarioPeriodo.getCargaHoraria());
            stmt.setInt(2, horarioPeriodo.getFuncionario().getCodFuncionario());
            stmt.setString(3, horarioPeriodo.getPeriodo().getCodAno());
            stmt.setInt(4, horarioPeriodo.getPeriodo().getCodMes());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(HorarioPeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean remover(HorarioPeriodo horarioPeriodo) {
        String sql = "DELETE FROM horario_periodo WHERE funcionario=? AND ano=? AND mes=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, horarioPeriodo.getFuncionario().getCodFuncionario());
            stmt.setString(2, horarioPeriodo.getPeriodo().getCodAno());
            stmt.setInt(3, horarioPeriodo.getPeriodo().getCodMes());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(HorarioPeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean removerPorFuncionario(Funcionario funcionario) {
        String sql = "DELETE FROM horario_periodo WHERE funcionario = ?;";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCodFuncionario());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(HorarioPeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public List<HorarioPeriodo> listar() {
        String sql = "SELECT * FROM horario_periodo";
        List<HorarioPeriodo> retorno = new ArrayList<>();
        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        funcionarioDAO.setConnection(this.connection);
        PeriodoDAO periodoDAO = new PeriodoDAO();
        periodoDAO.setConnection(this.connection);

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                HorarioPeriodo horarioPeriodo = new HorarioPeriodo();
                Funcionario funcionario = new Funcionario();
                Periodo periodo = new Periodo();

                funcionario.setCodFuncionario(resultado.getInt("funcionario"));
                horarioPeriodo.setFuncionario(funcionarioDAO.buscar(funcionario));

                periodo.setCodAno(resultado.getString("ano"));
                periodo.setCodMes(resultado.getInt("mes"));
                horarioPeriodo.setPeriodo(periodoDAO.buscar(periodo));

                horarioPeriodo.setCargaHoraria(resultado.getDouble("carga_horaria"));

                retorno.add(horarioPeriodo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HorarioPeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public HorarioPeriodo buscar(HorarioPeriodo horarioPeriodo) {
        String sql = "SELECT * FROM horario_periodo WHERE funcionario=? AND ano=? AND mes=?";
        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        PeriodoDAO periodoDAO = new PeriodoDAO();
        funcionarioDAO.setConnection(this.connection);
        periodoDAO.setConnection(this.connection);
        HorarioPeriodo retorno = new HorarioPeriodo();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, horarioPeriodo.getFuncionario().getCodFuncionario());
            stmt.setString(2, horarioPeriodo.getPeriodo().getCodAno());
            stmt.setInt(3, horarioPeriodo.getPeriodo().getCodMes());

            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Periodo periodo = new Periodo();
                Funcionario funcionario = new Funcionario();

                periodo.setCodAno(resultado.getString("ano"));
                periodo.setCodMes(resultado.getInt("mes"));
                horarioPeriodo.setPeriodo(periodoDAO.buscar(periodo));

                funcionario.setCodFuncionario(resultado.getInt("funcionario"));
                horarioPeriodo.setFuncionario(funcionarioDAO.buscar(funcionario));

                horarioPeriodo.setCargaHoraria(resultado.getDouble("carga_horaria"));

                retorno = horarioPeriodo;
            } else {
                retorno = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(HorarioPeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
}
