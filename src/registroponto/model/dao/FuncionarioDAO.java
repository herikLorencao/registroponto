package registroponto.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import registroponto.model.domain.Cargo;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.Setor;

public class FuncionarioDAO {

    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean inserir(Funcionario funcionario) {
        String sql = "INSERT INTO funcionario(cargo, nome, cpf, senha) "
                + "VALUES(?, ?, ?, ?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCargo().getCodCargo());
            stmt.setString(2, funcionario.getNome());
            stmt.setString(3, funcionario.getCpf());
            stmt.setString(4, funcionario.getSenha());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean alterar(Funcionario funcionario) {
        String sql = "UPDATE funcionario SET cargo=?, nome=?, cpf=?, senha=?"
                + " WHERE cod_funcionario=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCargo().getCodCargo());
            stmt.setString(2, funcionario.getNome());
            stmt.setString(3, funcionario.getCpf());
            stmt.setString(4, funcionario.getSenha());
            stmt.setInt(5, funcionario.getCodFuncionario());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean remover(Funcionario funcionario) {
        PontoDAO pontoDAO = new PontoDAO();
        HorarioPeriodoDAO horarioPeriodoDAO = new HorarioPeriodoDAO();
        
        pontoDAO.setConnection(connection);
        horarioPeriodoDAO.setConnection(connection);

        String sql = "DELETE FROM funcionario WHERE cod_funcionario=?";
        
        try {
            connection.setAutoCommit(false);
            
            pontoDAO.removerPorFuncionario(funcionario);
            horarioPeriodoDAO.removerPorFuncionario(funcionario);
            
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCodFuncionario());
            stmt.execute();
            
            connection.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean removerPorCargo(Cargo cargo) {
        

        String sql = "DELETE FROM funcionario WHERE cargo=?";
        
        try {
            connection.setAutoCommit(false);

            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, cargo.getCodCargo());
            stmt.execute();
            
            connection.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public List<Funcionario> listar() {
        String sql = "SELECT * FROM funcionario ORDER BY nome;";
        List<Funcionario> retorno = new ArrayList<>();
        CargoDAO cargoDAO = new CargoDAO();
        cargoDAO.setConnection(this.connection);

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Funcionario funcionario = new Funcionario();
                Cargo cargo = new Cargo();

                funcionario.setCodFuncionario(resultado.getInt("cod_funcionario"));

                cargo.setCodCargo(resultado.getInt("cargo"));
                funcionario.setCargo(cargoDAO.buscar(cargo));

                funcionario.setNome(resultado.getString("nome"));
                funcionario.setCpf(resultado.getString("cpf"));
                funcionario.setSenha(resultado.getString("senha"));

                retorno.add(funcionario);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public Funcionario buscar(Funcionario funcionario) {
        String sql = "SELECT * FROM funcionario WHERE cod_funcionario=?";
        CargoDAO cargoDAO = new CargoDAO();
        cargoDAO.setConnection(this.connection);
        Funcionario retorno = new Funcionario();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCodFuncionario());
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Cargo cargo = new Cargo();
                cargo.setCodCargo(resultado.getInt("cargo"));
                funcionario.setCargo(cargoDAO.buscar(cargo));

                funcionario.setNome(resultado.getString("nome"));
                funcionario.setCpf(resultado.getString("cpf"));
                funcionario.setSenha(resultado.getString("senha"));

                retorno = funcionario;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public List<Funcionario> buscarPorCargo(Cargo cargo) {
        String sql = "SELECT * FROM funcionario WHERE cargo=?;";
        List<Funcionario> retorno = new ArrayList<>();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, cargo.getCodCargo());
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Funcionario funcionario = new Funcionario();

                funcionario.setCodFuncionario(resultado.getInt("cod_funcionario"));

                funcionario.setCargo(cargo);

                funcionario.setNome(resultado.getString("nome"));
                funcionario.setCpf(resultado.getString("cpf"));
                funcionario.setSenha(resultado.getString("senha"));

                retorno.add(funcionario);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
    
    public Funcionario buscarCPFSenha(Funcionario funcionario) {
        String sql = "SELECT * FROM funcionario WHERE cpf=? AND senha=?";
        CargoDAO cargoDAO = new CargoDAO();
        cargoDAO.setConnection(this.connection);
        Funcionario retorno = new Funcionario();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, funcionario.getCpf());
            stmt.setString(2, funcionario.getSenha());
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Cargo cargo = new Cargo();
                cargo.setCodCargo(resultado.getInt("cargo"));
                funcionario.setCargo(cargoDAO.buscar(cargo));

                funcionario.setCodFuncionario(resultado.getInt("cod_funcionario"));
                funcionario.setNome(resultado.getString("nome"));

                retorno = funcionario;
            } else {
                retorno = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    
    public List<Funcionario> buscarPorSetor(Setor setor) {
        String sql = "SELECT * FROM funcionario f "
                + "INNER JOIN cargo c ON f.cargo = c.cod_cargo "
                + "WHERE c.setor = ? "
                + "ORDER BY f.nome;";

        List<Funcionario> retorno = new ArrayList<>();

        CargoDAO cargoDAO = new CargoDAO();
        cargoDAO.setConnection(this.connection);

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, setor.getCodSetor());
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Funcionario funcionario = new Funcionario();
                Cargo cargo = new Cargo();

                funcionario.setCodFuncionario(resultado.getInt("cod_funcionario"));

                cargo.setCodCargo(resultado.getInt("cargo"));
                funcionario.setCargo(cargoDAO.buscar(cargo));

                funcionario.setNome(resultado.getString("nome"));
                funcionario.setCpf(resultado.getString("cpf"));
                funcionario.setSenha(resultado.getString("senha"));

                retorno.add(funcionario);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public Map<String, ArrayList> buscarQuantidadeHorasTrabalhadas(Funcionario funcionario) {
        String sql = "SELECT ano, mes, carga_horaria FROM horario_periodo WHERE funcionario = ? ORDER BY mes;";
        Map<String, ArrayList> retorno = new HashMap();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCodFuncionario());
            ResultSet resultado = stmt.executeQuery();

            while (resultado.next()) {
                ArrayList linha = new ArrayList();
                if (!retorno.containsKey(resultado.getString("ano"))) {
                    linha.add(resultado.getInt("mes"));
                    linha.add(resultado.getDouble("carga_horaria"));
                    retorno.put(resultado.getString("ano"), linha);
                } else {
                    ArrayList linhaNova = retorno.get(resultado.getString("ano"));
                    linhaNova.add(resultado.getInt("mes"));
                    linhaNova.add(resultado.getDouble("carga_horaria"));
                }
            }
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
}
