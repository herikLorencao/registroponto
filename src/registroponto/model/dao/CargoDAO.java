package registroponto.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import registroponto.model.domain.Cargo;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.Setor;

public class CargoDAO {

    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public boolean inserir(Cargo cargo) {
        String sql = "INSERT INTO cargo(nome, setor, valor_hora_trabalho, nivel_sistema) "
                + "VALUES(?, ?, ?, ?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, cargo.getNome());
            stmt.setInt(2, cargo.getSetor().getCodSetor());
            stmt.setDouble(3, cargo.getValorHoraTrabalho());
            stmt.setInt(4, cargo.getNivelSistema());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CargoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean alterar(Cargo cargo) {
        String sql = "UPDATE cargo SET nome=?, setor=?, valor_hora_trabalho=?, nivel_sistema=?"
                + " WHERE cod_cargo=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, cargo.getNome());
            stmt.setInt(2, cargo.getSetor().getCodSetor());
            stmt.setDouble(3, cargo.getValorHoraTrabalho());
            stmt.setInt(4, cargo.getNivelSistema());
            stmt.setInt(5, cargo.getCodCargo());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CargoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean remover(Cargo cargo) {
        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        List<Funcionario> listFuncionario;

        funcionarioDAO.setConnection(connection);

        String sql = "DELETE FROM cargo WHERE cod_cargo=?";

        try {
            connection.setAutoCommit(false);
            listFuncionario = funcionarioDAO.buscarPorCargo(cargo);
            
            for (Funcionario funcionario : listFuncionario) {
                funcionarioDAO.remover(funcionario);
            }
            
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, cargo.getCodCargo());
            stmt.execute();

            connection.commit();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(CargoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public List<Cargo> listar() {
        String sql = "SELECT * FROM cargo ORDER BY nome;";
        List<Cargo> retorno = new ArrayList<>();
        SetorDAO setorDAO = new SetorDAO();
        setorDAO.setConnection(this.connection);

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Setor setor = new Setor();
                Cargo cargo = new Cargo();
                cargo.setCodCargo(resultado.getInt("cod_cargo"));
                cargo.setNome(resultado.getString("nome"));

                setor.setCodSetor(resultado.getInt("setor"));
                cargo.setSetor(setorDAO.buscar(setor));

                cargo.setNivelSistema(resultado.getInt("nivel_sistema"));
                cargo.setValorHoraTrabalho(resultado.getDouble("valor_hora_trabalho"));

                retorno.add(cargo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CargoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public Cargo buscar(Cargo cargo) {
        String sql = "SELECT * FROM cargo WHERE cod_cargo=?";
        SetorDAO setorDAO = new SetorDAO();
        setorDAO.setConnection(this.connection);
        Cargo retorno = new Cargo();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, cargo.getCodCargo());
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Setor setor = new Setor();
                cargo.setNome(resultado.getString("nome"));

                setor.setCodSetor(resultado.getInt("setor"));
                cargo.setSetor(setorDAO.buscar(setor));

                cargo.setValorHoraTrabalho(resultado.getDouble("valor_hora_trabalho"));
                cargo.setNivelSistema(resultado.getInt("nivel_sistema"));

                retorno = cargo;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CargoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
    
    public List<Cargo> buscarPorSetor(Setor setor) {
        String sql = "SELECT * FROM cargo WHERE setor=?;";
        
        List<Cargo> retorno = new ArrayList<>();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, setor.getCodSetor());
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Cargo cargo = new Cargo();
                cargo.setCodCargo(resultado.getInt("cod_cargo"));
                cargo.setNome(resultado.getString("nome"));

                cargo.setSetor(setor);

                cargo.setNivelSistema(resultado.getInt("nivel_sistema"));
                cargo.setValorHoraTrabalho(resultado.getDouble("valor_hora_trabalho"));

                retorno.add(cargo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CargoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
}
