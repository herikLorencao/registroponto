package registroponto.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.Periodo;
import registroponto.model.domain.Ponto;

public class PontoDAO {
    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
     public boolean inserir(Ponto ponto) {
        String sql = "INSERT INTO ponto(ano, mes, funcionario, hora_inicio, hora_fim) "
                + "VALUES(?, ?, ?, ?, ?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, ponto.getPeriodo().getCodAno());
            stmt.setInt(2, ponto.getPeriodo().getCodMes());
            stmt.setInt(3, ponto.getFuncionario().getCodFuncionario());
            stmt.setTime(4, Time.valueOf(ponto.getHoraInicio()));
            if (ponto.getHoraFim() != null) {
                stmt.setTime(5, Time.valueOf(ponto.getHoraFim()));
            } else {
                stmt.setNull(5, Types.TIME);
            }          
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PontoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
     
    public boolean alterar(Ponto ponto) {
        String sql = "UPDATE ponto SET ano=?, mes=?, funcionario=?, hora_inicio=?, hora_fim=?"
                + " WHERE cod_ponto=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, ponto.getPeriodo().getCodAno());
            stmt.setInt(2, ponto.getPeriodo().getCodMes());
            stmt.setInt(3, ponto.getFuncionario().getCodFuncionario());
            stmt.setTime(4, Time.valueOf(ponto.getHoraInicio()));
            stmt.setTime(5, Time.valueOf(ponto.getHoraFim()));
            stmt.setInt(6, ponto.getCodPonto());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PontoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean remover(Ponto ponto) {
        String sql = "DELETE FROM ponto WHERE cod_ponto=?";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, ponto.getCodPonto());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PontoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
     public boolean removerPorFuncionario(Funcionario funcionario) {
        String sql = "DELETE FROM ponto WHERE funcionario = ?;";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCodFuncionario());
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(HorarioPeriodoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public List<Ponto> listar() {
        String sql = "SELECT * FROM ponto";
        List<Ponto> retorno = new ArrayList<>();
        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        funcionarioDAO.setConnection(this.connection);
        PeriodoDAO periodoDAO = new PeriodoDAO();
        periodoDAO.setConnection(this.connection);

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            ResultSet resultado = stmt.executeQuery();
            while (resultado.next()) {
                Ponto ponto = new Ponto();
                Funcionario funcionario = new Funcionario();
                Periodo periodo = new Periodo();
              
                ponto.setCodPonto(resultado.getInt("cod_ponto"));
                
                periodo.setCodAno(resultado.getString("ano"));
                periodo.setCodMes(resultado.getInt("mes"));
                ponto.setPeriodo(periodoDAO.buscar(periodo));

                funcionario.setCodFuncionario(resultado.getInt("funcionario"));
                ponto.setFuncionario(funcionarioDAO.buscar(funcionario));
                
                ponto.setHoraInicio(resultado.getTime("hora_inicio").toLocalTime());
                ponto.setHoraFim(resultado.getTime("hora_fim").toLocalTime());
                
                retorno.add(ponto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PontoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
    
    public Ponto buscar(Ponto ponto) {
        String sql = "SELECT * FROM ponto WHERE cod_ponto=?";
        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        PeriodoDAO periodoDAO = new PeriodoDAO();
        funcionarioDAO.setConnection(this.connection);
        periodoDAO.setConnection(this.connection);
        Ponto retorno = new Ponto();

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, ponto.getCodPonto());
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {
                Periodo periodo = new Periodo();
                Funcionario funcionario = new Funcionario();
                
                ponto.setCodPonto(resultado.getInt("cod_ponto"));
                
                periodo.setCodAno(resultado.getString("ano"));
                periodo.setCodMes(resultado.getInt("mes"));
                ponto.setPeriodo(periodoDAO.buscar(periodo));

                funcionario.setCodFuncionario(resultado.getInt("funcionario"));
                ponto.setFuncionario(funcionarioDAO.buscar(funcionario));

                ponto.setHoraInicio(resultado.getTime("hora_inicio").toLocalTime());
                
                if (resultado.getTime("hora_fim") != null) {
                    ponto.setHoraFim(resultado.getTime("hora_fim").toLocalTime());
                } else {
                    ponto.setHoraFim(null);
                }

                retorno = ponto;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PontoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
    
    public int buscarUltimoCodPontoFuncionario(Funcionario funcionario) {
        String sql = "SELECT max(p.cod_ponto) AS cod_ultimo_ponto"
                + "     FROM ponto p"
                + "     INNER JOIN funcionario f ON p.funcionario = f.cod_funcionario"
                + "     WHERE f.cod_funcionario = ?;";
       
        int retorno = 0;

        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, funcionario.getCodFuncionario());
            ResultSet resultado = stmt.executeQuery();
            if (resultado.next()) {          
                retorno = resultado.getInt("cod_ultimo_ponto");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PontoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }
}
