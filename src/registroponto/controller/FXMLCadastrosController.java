package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import registroponto.utils.ItemMenu;

public class FXMLCadastrosController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ListView<ItemMenu> listViewTipoCadastro;
    @FXML
    private Button buttonInserir;
    @FXML
    private Button buttonAlterar;
    @FXML
    private Button buttonRemover;

    private final List<ItemMenu> listItensMenu = new ArrayList<>();
    private ObservableList<ItemMenu> observableListItensMenu;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        carregarListViewTipoCadastro();
    }

    public void carregarListViewTipoCadastro() {
        ItemMenu itemMenuFuncionario = new ItemMenu("Funcionário", "/registroponto/view/FXMLCadastroFuncionario");
        ItemMenu itemMenuCargo = new ItemMenu("Cargo", "/registroponto/view/FXMLCadastroCargo");
        ItemMenu itemMenuSetor = new ItemMenu("Setor", "/registroponto/view/FXMLCadastroSetor");

        listItensMenu.add(itemMenuFuncionario);
        listItensMenu.add(itemMenuCargo);
        listItensMenu.add(itemMenuSetor);

        observableListItensMenu = FXCollections.observableArrayList(listItensMenu);
        listViewTipoCadastro.setItems(observableListItensMenu);

        listViewTipoCadastro.getSelectionModel().select(itemMenuFuncionario);

    }

    public void selecionarItemListViewTipoCadastro(ItemMenu itemMenu) {
        try {
            AnchorPane a = FXMLLoader.load(getClass().getResource(itemMenu.getDestino()));
            anchorPane.getChildren().setAll(a);
        } catch (IOException ex) {
            Logger.getLogger(FXMLCadastrosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void handleButtonInserir() {
        abrirSubMenu("Inserir");
    }

    public void handleButtonAlterar() {
        abrirSubMenu("Alterar");
    }

    public void handleButtonRemover() {
        abrirSubMenu("Remover");
    }

    public void abrirSubMenu(String operacao) {
        ItemMenu itemMenu = listViewTipoCadastro.getSelectionModel().getSelectedItem();
        itemMenu.setDestino(itemMenu.getDestino() + operacao + ".fxml");
        selecionarItemListViewTipoCadastro(itemMenu);
    }
}
