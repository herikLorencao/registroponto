package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import registroponto.utils.ItemMenu;
import registroponto.utils.Sessao;
import registroponto.utils.Usuario;

public class FXMLTelaInicialController implements Initializable {

    @FXML
    private AnchorPane anchorPaneBase;
    @FXML
    private AnchorPane anchorPaneMenu;
    @FXML
    private AnchorPane anchorPaneConteudo;
    @FXML
    private ListView<ItemMenu> listViewMenu;
    @FXML
    private Button buttonSair;

    private final List<ItemMenu> listItensMenu = new ArrayList<>();
    private ObservableList<ItemMenu> observableListItensMenu;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        carregarListViewMenu();

        listViewMenu.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> selecionarItemMenu(newValue));
    }

    public void carregarListViewMenu() {
        Sessao sessao = Sessao.getInstance();
        Usuario usuario = sessao.getUsuario();

        ItemMenu menuInicio = new ItemMenu("Inicio", "/registroponto/view/FXMLDadosFuncionario.fxml");
        ItemMenu menuCadastros = new ItemMenu("Cadastros", "/registroponto/view/FXMLCadastros.fxml");
        ItemMenu menuRegistrarPonto = new ItemMenu("Registrar Ponto", "/registroponto/view/FXMLRegistroPonto.fxml");
        ItemMenu menuGraficos = new ItemMenu("Gráficos", "/registroponto/view/FXMLGraficos.fxml");
        ItemMenu menuRelatorios = new ItemMenu("Relatórios", "/registroponto/view/FXMLRelatorios.fxml");

        listItensMenu.add(menuInicio);

        if (usuario.getNivelSistema() == 3) {
            listItensMenu.add(menuCadastros);
        }

        listItensMenu.add(menuRegistrarPonto);
        listItensMenu.add(menuGraficos);
        listItensMenu.add(menuRelatorios);

        observableListItensMenu = FXCollections.observableArrayList(listItensMenu);
        listViewMenu.setItems(observableListItensMenu);

        listViewMenu.getSelectionModel().select(menuInicio);
        selecionarItemMenu(menuInicio);
    }

    public void selecionarItemMenu(ItemMenu itemMenuNovo) {

        try {
            AnchorPane novo = FXMLLoader.load(getClass().getResource(itemMenuNovo.getDestino()));
            anchorPaneConteudo.getChildren().setAll(novo);

        } catch (IOException ex) {
            Logger.getLogger(FXMLTelaInicialController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void handleButtonSair() {
        try {
            AnchorPane tela = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLTelaLogin.fxml"));
            anchorPaneBase.getChildren().setAll(tela);
        } catch (IOException ex) {
            Logger.getLogger(FXMLTelaInicialController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Sessao.endSession();
    }
}
