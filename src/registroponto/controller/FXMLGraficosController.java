package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.FuncionarioDAO;
import registroponto.model.dao.SetorDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.Setor;
import registroponto.utils.Sessao;
import registroponto.utils.Usuario;

public class FXMLGraficosController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ComboBox<Setor> comboBoxSetores;
    @FXML
    private ListView<Funcionario> listViewFuncionarios;
    @FXML
    private Button buttonExibir;

    protected static Funcionario funcionarioGrafico;
    private final Usuario usuario = Sessao.getInstance().getUsuario();

    private List<Setor> listSetores = new ArrayList<>();
    private ObservableList<Setor> observableListSetores;
    private List<Funcionario> listFuncionarios = new ArrayList<>();
    private ObservableList<Funcionario> observableListFuncionarios;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
    private final SetorDAO setorDAO = new SetorDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        funcionarioDAO.setConnection(connection);
        setorDAO.setConnection(connection);
        carregarComboBoxSetor();

        comboBoxSetores.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> carregarListViewFuncionarios(newValue));
    }

    public void carregarComboBoxSetor() {
        if (usuario.getNivelSistema() == 3) {
            listSetores = setorDAO.listar();
        } else {
            listSetores.add(usuario.getFuncionario().getCargo().getSetor());
        }

        observableListSetores = FXCollections.observableArrayList(listSetores);
        comboBoxSetores.setItems(observableListSetores);
    }

    public void carregarListViewFuncionarios(Setor setor) {

        if (usuario.getNivelSistema() == 1) {
            listFuncionarios.add(funcionarioDAO.buscar(usuario.getFuncionario()));
        } else {
            listFuncionarios = funcionarioDAO.buscarPorSetor(setor);
        }

        observableListFuncionarios = FXCollections.observableArrayList(listFuncionarios);
        listViewFuncionarios.setItems(observableListFuncionarios);
    }

    @FXML
    private void handleButtonExibir(ActionEvent event) {
        if (validarEntradaDados()) {
            funcionarioGrafico = listViewFuncionarios.getSelectionModel().getSelectedItem();
            abrirGrafico();
        }
    }

    public void abrirGrafico() {
        try {
            AnchorPane tela = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLGraficoFuncionario.fxml"));
            anchorPane.getChildren().setAll(tela);
        } catch (IOException ex) {
            Logger.getLogger(FXMLGraficosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (comboBoxSetores.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione o setor do funcionário\n";
        }
        if (listViewFuncionarios.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione o funcionário";
        }
        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
