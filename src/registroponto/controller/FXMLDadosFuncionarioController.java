package registroponto.controller;

import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import registroponto.model.dao.FuncionarioDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Funcionario;
import registroponto.utils.Sessao;
import registroponto.utils.Usuario;

public class FXMLDadosFuncionarioController implements Initializable {

    @FXML
    private Label labelNome;
    @FXML
    private Label labelCPF;
    @FXML
    private Label labelCargo;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Sessao sessao = Sessao.getInstance();
        Usuario usuario = sessao.getUsuario();
        carregarDadosFuncionario(usuario.getFuncionario());
    }

    public void carregarDadosFuncionario(Funcionario funcionario) {
        labelNome.setText(funcionario.getNome());
        labelCPF.setText(colocarMascaraCPF(funcionario.getCpf()));
        labelCargo.setText(funcionario.getCargo().getNome());
    }

    public String colocarMascaraCPF(String cpf) {
        ArrayList<String> partesCPF = new ArrayList<>();
        String cpfComMascara = "";

        partesCPF.add(cpf.substring(0, 3) + ".");
        partesCPF.add(cpf.substring(3, 6) + ".");
        partesCPF.add(cpf.substring(6, 9) + "-");
        partesCPF.add(cpf.substring(9, 11));

        cpfComMascara = partesCPF.stream().map((partes) -> partes).reduce(cpfComMascara, String::concat);
        return cpfComMascara;
    }
}
