package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.SetorDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Setor;

public class FXMLCadastroSetorRemoverController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ListView<Setor> listViewSetores;
    @FXML
    private Button buttonCancelar;
    @FXML
    private Button buttonConfirmar;

    private List<Setor> listSetores;
    private ObservableList<Setor> observableListSetores;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final SetorDAO setorDAO = new SetorDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setorDAO.setConnection(connection);
        carregarListViewSetores();
    }

    public void carregarListViewSetores() {
        listSetores = setorDAO.listar();
        observableListSetores = FXCollections.observableArrayList(listSetores);

        listViewSetores.setItems(observableListSetores);
    }

    @FXML
    private void handleButtonConfirmar(ActionEvent event) {
        if (validarEntradaDados()) {
            Setor setor = listViewSetores.getSelectionModel().getSelectedItem();

            if (setorDAO.remover(setor)) {
                alertSucesso();
            } else {
                alertErro();
            }

            voltarTela();
        }
    }

    @FXML
    private void handleButtonCancelar(ActionEvent event) {
        voltarTela();
    }

    public void voltarTela() {
        try {
            AnchorPane a = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLCadastros.fxml"));
            anchorPane.getChildren().setAll(a);
        } catch (IOException ex) {
            Logger.getLogger(FXMLCadastroSetorInserirController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void alertSucesso() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Remoção de Setor");
        alert.setContentText("Setor removido com sucesso!");
        alert.show();
    }

    public void alertErro() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Remoção de Setor");
        alert.setContentText("Não foi possível remover o setor!");
        alert.show();
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (listViewSetores.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione o setor para remoção\n";
        }

        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
