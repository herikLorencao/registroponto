package registroponto.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import registroponto.model.dao.HorarioPeriodoDAO;
import registroponto.model.dao.PeriodoDAO;
import registroponto.model.dao.PontoDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.HorarioPeriodo;
import registroponto.model.domain.Periodo;
import registroponto.model.domain.Ponto;
import registroponto.utils.Sessao;

public class FXMLRegistroPontoController implements Initializable {

    @FXML
    private Button buttonRegistrar;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final PontoDAO pontoDAO = new PontoDAO();
    private final PeriodoDAO periodoDAO = new PeriodoDAO();
    private final HorarioPeriodoDAO horarioPeriodoDAO = new HorarioPeriodoDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pontoDAO.setConnection(connection);
        periodoDAO.setConnection(connection);
        horarioPeriodoDAO.setConnection(connection);
        definirBotao(Sessao.getInstance().getUsuario().getFuncionario());
    }

    @FXML
    private void handleButtonRegistro(ActionEvent event) {
        Ponto ponto;
        Ponto pontoPesquisa = new Ponto();
        LocalTime horario = LocalTime.now();
        Funcionario funcionario = Sessao.getInstance().getUsuario().getFuncionario();
        int codUltimoPonto = pontoDAO.buscarUltimoCodPontoFuncionario(funcionario);

        if (codUltimoPonto == 0) {
            // Primeiro ponto do funcionario
            definirPonto(funcionario, horario, null);

        } else {
            pontoPesquisa.setCodPonto(codUltimoPonto);
            ponto = pontoDAO.buscar(pontoPesquisa);

            if (ponto.getHoraFim() == null) {
                // Fechar Ponto
                definirPonto(funcionario, ponto.getHoraInicio(), horario);
            } else {
                // Abrir Ponto
                definirPonto(funcionario, horario, null);
            }
        }
        definirBotao(funcionario);
    }

    public void definirPonto(Funcionario funcionario, LocalTime horarioInicio, LocalTime horarioFim) {
        Ponto ponto = new Ponto();

        try {
            connection.setAutoCommit(false);

            ponto.setPeriodo(geraPeriodo());
            ponto.setFuncionario(funcionario);

            if (horarioFim == null) {
                ponto.setHoraInicio(horarioInicio);
                ponto.setHoraFim(null);
                pontoDAO.inserir(ponto);
            } else {
                ponto.setCodPonto(pontoDAO.buscarUltimoCodPontoFuncionario(funcionario));
                ponto.setHoraInicio(horarioInicio);
                ponto.setHoraFim(horarioFim);
                pontoDAO.alterar(ponto);
                atualizaHorarioPeriodo(funcionario, ponto.getPeriodo(), ponto);
            }
            connection.commit();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLRegistroPontoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Periodo geraPeriodo() {
        Periodo periodoAtual = new Periodo();
        Calendar data = Calendar.getInstance(Locale.ROOT);
        String label;

        periodoAtual.setCodAno(String.valueOf(data.get(Calendar.YEAR)));
        periodoAtual.setCodMes(data.get(Calendar.MONTH) + 1);
        label = String.valueOf(data.get(Calendar.YEAR)) + '/' + String.valueOf(data.get(Calendar.MONTH) + 1);
        periodoAtual.setLabel(label);

        if (periodoDAO.buscar(periodoAtual) == null) {
            periodoDAO.inserir(periodoAtual);
        }

        return periodoAtual;
    }

    public void atualizaHorarioPeriodo(Funcionario funcionario, Periodo periodo, Ponto ponto) {
        HorarioPeriodo horarioPeriodo = new HorarioPeriodo();
        double horarioAntigo;
        double horarioNovo;

        horarioPeriodo.setFuncionario(funcionario);
        horarioPeriodo.setPeriodo(periodo);

        if (horarioPeriodoDAO.buscar(horarioPeriodo) == null) {
            horarioPeriodoDAO.inserir(horarioPeriodo);
        } else {
            horarioAntigo = horarioPeriodoDAO.buscar(horarioPeriodo).getCargaHoraria();
            horarioPeriodo.setCargaHoraria(converterHorario(ponto) + horarioAntigo);
            horarioPeriodoDAO.alterar(horarioPeriodo);
        }
    }

    public double converterHorario(Ponto ponto) {
        double minutos = ChronoUnit.MINUTES.between(ponto.getHoraInicio(), ponto.getHoraFim());
        return minutos / 60;
    }

    public void definirBotao(Funcionario funcionario) {
        int codUltimoPonto = pontoDAO.buscarUltimoCodPontoFuncionario(funcionario);
        Ponto pontoPesquisa = new Ponto();
        Ponto ponto;

        pontoPesquisa.setCodPonto(codUltimoPonto);
        ponto = pontoDAO.buscar(pontoPesquisa);

        List<String> classesCss = new ArrayList<>();
        classesCss.add(0, "registrar-ponto");
        classesCss.add(1, "fechar-ponto");

        if (ponto.getHoraFim() == null) {
            // Fechar Ponto
            buttonRegistrar.getStyleClass().setAll(classesCss.get(1));
        } else {
            // Abrir Ponto
            buttonRegistrar.getStyleClass().setAll(classesCss.get(0));
        }
    }
}
