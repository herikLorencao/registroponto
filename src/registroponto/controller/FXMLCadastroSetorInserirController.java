package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.SetorDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Setor;

public class FXMLCadastroSetorInserirController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextField textFieldNome;
    @FXML
    private Button buttonCancelar;
    @FXML
    private Button buttonConfirmar;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final SetorDAO setorDAO = new SetorDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setorDAO.setConnection(connection);
    }

    public void handleButtonConfirmar() {
        if (validarEntradaDados()) {
            Setor setor = new Setor();
            setor.setNome(textFieldNome.getText());

            if (setorDAO.inserir(setor)) {
                alertSucesso();
            } else {
                alertErro();
            }

            voltarTela();
        }
    }

    public void handleButtonCancelar() {
        voltarTela();
    }

    public void voltarTela() {
        try {
            AnchorPane a = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLCadastros.fxml"));
            anchorPane.getChildren().setAll(a);
        } catch (IOException ex) {
            Logger.getLogger(FXMLCadastroSetorInserirController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void alertSucesso() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Cadastro de Setor");
        alert.setContentText("Setor cadastrado com sucesso!");
        alert.show();
    }

    public void alertErro() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Cadastro de Setor");
        alert.setContentText("Não foi possível cadastrar o setor!");
        alert.show();
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (textFieldNome.getText().equals("")) {
            informacoesValidacao += "Informe o nome\n";
        }

        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
