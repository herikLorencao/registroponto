package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.FuncionarioDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Funcionario;
import registroponto.utils.Sessao;
import registroponto.utils.TextFormat;
import registroponto.utils.Usuario;

public class FXMLTelaLoginController implements Initializable {

    @FXML
    private AnchorPane anchorPaneBase;
    @FXML
    private TextField textFieldCPF;
    @FXML
    private Hyperlink hiperlinkEsqueceuSenha;
    @FXML
    private PasswordField textFieldSenha;
    @FXML
    private Button buttonEntrar;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        funcionarioDAO.setConnection(connection);

    }

    public void carregarTelaInicial() {
        try {
            AnchorPane tela = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLTelaInicial.fxml"));
            anchorPaneBase.getChildren().setAll(tela);
        } catch (IOException ex) {
            Logger.getLogger(FXMLTelaLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void handleButtonEntrar() {
        if (validarEntradaDados()) {
            Sessao sessao = Sessao.getInstance();
            Usuario usuario = new Usuario();
            Funcionario funcionarioPesquisa = new Funcionario();
            Funcionario funcionario;

            funcionarioPesquisa.setCpf(tirarMascaraCPF(textFieldCPF.getText()));
            funcionarioPesquisa.setSenha(textFieldSenha.getText());

            funcionario = funcionarioDAO.buscarCPFSenha(funcionarioPesquisa);

            if (funcionario == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Login Incorreto");
                alert.setContentText("O cpf ou senha estão incorretos");
                alert.show();
            } else {
                usuario.setFuncionario(funcionario);
                usuario.setNivelSistema(funcionario.getCargo().getNivelSistema());
                sessao.setUsuario(usuario);
                carregarTelaInicial();
            }
        }
    }

    @FXML
    public void handleLinkEsqueceuSenha() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Esqueceu a senha?");
        alert.setContentText("Entre em contato com o gerente do seu setor");
        alert.show();
    }

    public void formateCPF() {
        TextFormat cpf = new TextFormat();
        cpf.setMask("###.###.###-##");
        cpf.setCaracteresValidos("0123456789");
        cpf.setTf(textFieldCPF);
        cpf.formatter();
    }

    public String tirarMascaraCPF(String cpf) {
        String aux = cpf.replace(".", "");
        return aux.replace("-", "");
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (textFieldCPF.getText().equals("")) {
            informacoesValidacao += "Informe o cpf do seu usuário\n";
        }
        if (textFieldSenha.getText().equals("")) {
            informacoesValidacao += "Informe a senha do seu usuário";
        }
        if (tirarMascaraCPF(textFieldCPF.getText()).contains(" ")) {
            informacoesValidacao += "Valor de CPF inválido\n";
        }

        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Campos de login com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
