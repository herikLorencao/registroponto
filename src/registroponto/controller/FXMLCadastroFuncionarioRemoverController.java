package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.FuncionarioDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Funcionario;

public class FXMLCadastroFuncionarioRemoverController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Button buttonCancelar;
    @FXML
    private Button buttonConfirmar;
    @FXML
    private ListView<Funcionario> listViewFuncionarios;

    private List<Funcionario> listFuncionarios;
    private ObservableList<Funcionario> observableListFuncionarios;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        funcionarioDAO.setConnection(connection);
        carregarListViewFuncionarios();
    }

    public void carregarListViewFuncionarios() {
        listFuncionarios = funcionarioDAO.listar();
        observableListFuncionarios = FXCollections.observableArrayList(listFuncionarios);
        listViewFuncionarios.setItems(observableListFuncionarios);
    }

    @FXML
    private void handleButtonCancelar(ActionEvent event) {
        voltarTela();
    }

    @FXML
    private void handleButtonConfirmar(ActionEvent event) {
        if (validarEntradaDados()) {
            Funcionario funcionario = listViewFuncionarios.getSelectionModel().getSelectedItem();

            if (funcionarioDAO.remover(funcionario)) {
                alertSucesso();
            } else {
                alertErro();
            }

            voltarTela();
        }
    }

    public void voltarTela() {
        try {
            AnchorPane a = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLCadastros.fxml"));
            anchorPane.getChildren().setAll(a);
        } catch (IOException ex) {
            Logger.getLogger(FXMLCadastroSetorInserirController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void alertSucesso() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Remoção de Funcionário");
        alert.setContentText("Funcionário removido com sucesso!");
        alert.show();
    }

    public void alertErro() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Remoção de Funcionário");
        alert.setContentText("Não foi possível remover o funcionário!");
        alert.show();
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (listViewFuncionarios.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione um funcionário para remoção\n";
        }

        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
