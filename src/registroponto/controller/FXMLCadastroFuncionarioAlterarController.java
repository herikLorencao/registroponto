package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.CargoDAO;
import registroponto.model.dao.FuncionarioDAO;
import registroponto.model.dao.SetorDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Cargo;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.Setor;
import registroponto.utils.TextFormat;

public class FXMLCadastroFuncionarioAlterarController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Button buttonCancelar;
    @FXML
    private Button buttonConfirmar;
    @FXML
    private ListView<Cargo> listViewCargos;
    @FXML
    private TextField textFieldNome;
    @FXML
    private TextField textFieldCPF;
    @FXML
    private TextField textFieldSenha;
    @FXML
    private ListView<Funcionario> listViewFuncionarios;

    private List<Cargo> listCargos;
    private List<Funcionario> listFuncionarios;
    private ObservableList<Cargo> observableListCargos;
    private ObservableList<Funcionario> observableListFuncionarios;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final CargoDAO cargoDAO = new CargoDAO();
    private final SetorDAO setorDAO = new SetorDAO();
    private final FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargoDAO.setConnection(connection);
        setorDAO.setConnection(connection);
        funcionarioDAO.setConnection(connection);

        carregarListViewFuncionarios();
        carregarListViewCargos();

        listViewFuncionarios.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> selecionarListViewFuncionarios(newValue));
    }

    public void selecionarListViewFuncionarios(Funcionario funcionario) {
        textFieldNome.setText(funcionario.getNome());
        textFieldCPF.setText(colocarMascaraCPF(funcionario.getCpf()));
        textFieldSenha.setText(funcionario.getSenha());
    }

    public void carregarListViewFuncionarios() {
        listFuncionarios = funcionarioDAO.listar();
        observableListFuncionarios = FXCollections.observableArrayList(listFuncionarios);
        listViewFuncionarios.setItems(observableListFuncionarios);
    }

    public void carregarListViewCargos() {
        listCargos = cargoDAO.listar();

        observableListCargos = FXCollections.observableArrayList(listCargos);
        listViewCargos.setItems(observableListCargos);
    }

    @FXML
    private void handleButtonCancelar(ActionEvent event) {
        voltarTela();
    }

    @FXML
    private void handleButtonConfirmar(ActionEvent event) {
        if (validarEntradaDados()) {
            Funcionario funcionario = new Funcionario();

            funcionario.setCodFuncionario(listViewFuncionarios.getSelectionModel().getSelectedItem().getCodFuncionario());
            funcionario.setNome(textFieldNome.getText());
            funcionario.setCargo(listViewCargos.getSelectionModel().getSelectedItem());
            funcionario.setCpf(tirarMascaraCPF(textFieldCPF.getText()));
            funcionario.setSenha(textFieldSenha.getText());

            if (funcionarioDAO.alterar(funcionario)) {
                alertSucesso();
            } else {
                alertErro();
            }

            voltarTela();
        }
    }

    public void voltarTela() {
        try {
            AnchorPane a = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLCadastros.fxml"));
            anchorPane.getChildren().setAll(a);
        } catch (IOException ex) {
            Logger.getLogger(FXMLCadastroCargoInserirController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void alertSucesso() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Alteração de Funcionário");
        alert.setContentText("Funcionário alterado com sucesso!");
        alert.show();
    }

    public void alertErro() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Alteração de Funcionário");
        alert.setContentText("Não foi possível alterar o funcionário!");
        alert.show();
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (listViewFuncionarios.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione um funcionário para alteração\n";
        }
        if (textFieldNome.getText().equals("")) {
            informacoesValidacao += "Informe um nome\n";
        }
        if (textFieldCPF.getText().equals("")) {
            informacoesValidacao += "Informe o CPF\n";
        } else {
            if (tirarMascaraCPF(textFieldCPF.getText()).contains(" ")) {
                informacoesValidacao += "Valor de CPF inválido\n";
            }
        }
        if (textFieldSenha.getText().equals("")) {
            informacoesValidacao += "Informe a senha\n";
        }
        if (listViewCargos.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione um cargo";
        }

        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }

    public void formateCPF() {
        TextFormat cpf = new TextFormat();
        cpf.setMask("###.###.###-##");
        cpf.setCaracteresValidos("0123456789");
        cpf.setTf(textFieldCPF);
        cpf.formatter();
    }

    public String tirarMascaraCPF(String cpf) {
        String aux = cpf.replace(".", "");
        return aux.replace("-", "");
    }

    public String colocarMascaraCPF(String cpf) {
        ArrayList<String> partesCPF = new ArrayList<>();
        String cpfComMascara = "";

        partesCPF.add(cpf.substring(0, 3) + ".");
        partesCPF.add(cpf.substring(3, 6) + ".");
        partesCPF.add(cpf.substring(6, 9) + "-");
        partesCPF.add(cpf.substring(9, 11));

        cpfComMascara = partesCPF.stream().map((partes) -> partes).reduce(cpfComMascara, String::concat);
        return cpfComMascara;
    }
}
