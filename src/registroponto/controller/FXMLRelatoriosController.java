package registroponto.controller;

import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import registroponto.model.dao.FuncionarioDAO;
import registroponto.model.dao.SetorDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Funcionario;
import registroponto.model.domain.Setor;
import registroponto.utils.Sessao;
import registroponto.utils.Usuario;

public class FXMLRelatoriosController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ComboBox<Setor> comboBoxSetores;
    @FXML
    private ListView<Funcionario> listViewFuncionarios;
    @FXML
    private Button buttonImprimir;

    private final Usuario usuario = Sessao.getInstance().getUsuario();

    private List<Setor> listSetores = new ArrayList<>();
    private ObservableList<Setor> observableListSetores;
    private List<Funcionario> listFuncionarios = new ArrayList<>();
    private ObservableList<Funcionario> observableListFuncionarios;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
    private final SetorDAO setorDAO = new SetorDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        funcionarioDAO.setConnection(connection);
        setorDAO.setConnection(connection);
        carregarComboBoxSetor();

        comboBoxSetores.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> carregarListViewFuncionarios(newValue));
    }

    public void carregarComboBoxSetor() {
        if (usuario.getNivelSistema() == 3) {
            listSetores = setorDAO.listar();
        } else {
            listSetores.add(usuario.getFuncionario().getCargo().getSetor());
        }

        observableListSetores = FXCollections.observableArrayList(listSetores);
        comboBoxSetores.setItems(observableListSetores);
    }

    public void carregarListViewFuncionarios(Setor setor) {

        if (usuario.getNivelSistema() == 1) {
            listFuncionarios.add(funcionarioDAO.buscar(usuario.getFuncionario()));
        } else {
            listFuncionarios = funcionarioDAO.buscarPorSetor(setor);
        }

        observableListFuncionarios = FXCollections.observableArrayList(listFuncionarios);
        listViewFuncionarios.setItems(observableListFuncionarios);
    }

    @FXML
    private void handleButtonImprimir(ActionEvent event) {
        if (validarEntradaDados()) {
            Funcionario funcionarioRelatorio = listViewFuncionarios.getSelectionModel().getSelectedItem();
            try {
                exibirRelatorio(funcionarioRelatorio);
            } catch (JRException ex) {
                Logger.getLogger(FXMLRelatoriosController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void exibirRelatorio(Funcionario funcionario) throws JRException {
        HashMap filtro = new HashMap();
        filtro.put("cdFuncionario", funcionario.getCodFuncionario());
        URL url = getClass().getResource("/registroponto/relatorios/RegistroPontoRelatorioFuncionario.jasper");
        JasperReport report = (JasperReport) JRLoader.loadObject(url);
        JasperPrint print = JasperFillManager.fillReport(report, filtro, connection);
        JasperViewer viewer = new JasperViewer(print, false);
        viewer.setVisible(true);
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (comboBoxSetores.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione o setor do funcionário\n";
        }
        if (listViewFuncionarios.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione o funcionário";
        }
        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
