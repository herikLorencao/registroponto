package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.CargoDAO;
import registroponto.model.dao.SetorDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Cargo;
import registroponto.model.domain.Setor;
import registroponto.utils.ItemComboBox;

public class FXMLCadastroCargoInserirController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextField textFieldNome;
    @FXML
    private Button buttonCancelar;
    @FXML
    private Button buttonConfirmar;
    @FXML
    private TextField textFieldValorHoraTrabalho;
    @FXML
    private ComboBox<ItemComboBox> comboBoxNivelSistema;
    @FXML
    private ComboBox<Setor> comboBoxSetor;

    private final List<ItemComboBox> listNiveisSistema = new ArrayList<>();
    private List<Setor> listSetores;
    private ObservableList<ItemComboBox> observableListItensComboBox;
    private ObservableList<Setor> observableListSetores;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final CargoDAO cargoDAO = new CargoDAO();
    private final SetorDAO setorDAO = new SetorDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargoDAO.setConnection(connection);
        setorDAO.setConnection(connection);

        carregarComboBoxNivelSistema();
        carregarComboBoxSetores();
    }

    public void carregarComboBoxNivelSistema() {
        ItemComboBox itemComboBox1 = new ItemComboBox("Administrador", 3);
        ItemComboBox itemComboBox2 = new ItemComboBox("Gerencial", 2);
        ItemComboBox itemComboBox3 = new ItemComboBox("Basico", 1);

        listNiveisSistema.add(itemComboBox1);
        listNiveisSistema.add(itemComboBox2);
        listNiveisSistema.add(itemComboBox3);

        observableListItensComboBox = FXCollections.observableArrayList(listNiveisSistema);
        comboBoxNivelSistema.setItems(observableListItensComboBox);
    }

    public void carregarComboBoxSetores() {
        listSetores = setorDAO.listar();
        observableListSetores = FXCollections.observableArrayList(listSetores);
        comboBoxSetor.setItems(observableListSetores);
    }

    @FXML
    private void handleButtonCancelar(ActionEvent event) {
        voltarTela();
    }

    @FXML
    private void handleButtonConfirmar(ActionEvent event) {
        if (validarEntradaDados()) {
            Cargo cargo = new Cargo();
            cargo.setNome(textFieldNome.getText());
            cargo.setSetor(comboBoxSetor.getSelectionModel().getSelectedItem());
            cargo.setValorHoraTrabalho(Double.parseDouble(textFieldValorHoraTrabalho.getText()));
            cargo.setNivelSistema(comboBoxNivelSistema.getSelectionModel().getSelectedItem().getValor());

            if (cargoDAO.inserir(cargo)) {
                alertSucesso();
            } else {
                alertErro();
            }

            voltarTela();
        }
    }

    public void voltarTela() {
        try {
            AnchorPane a = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLCadastros.fxml"));
            anchorPane.getChildren().setAll(a);
        } catch (IOException ex) {
            Logger.getLogger(FXMLCadastroCargoInserirController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void alertSucesso() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Cadastro de Cargo");
        alert.setContentText("Cargo cadastrado com sucesso!");
        alert.show();
    }

    public void alertErro() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Cadastro de Cargo");
        alert.setContentText("Não foi possível cadastrar o cargo!");
        alert.show();
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (textFieldNome.getText().equals("")) {
            informacoesValidacao += "Informe um nome\n";
        }
        if (comboBoxSetor.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione um setor\n";
        }
        if (comboBoxNivelSistema.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione o nível de sistema\n";
        }
        if (textFieldValorHoraTrabalho.getText().equals("")) {
            informacoesValidacao += "Defina o valor da hora de trabalho\n";
        } else {
            try {
                Double.parseDouble(textFieldValorHoraTrabalho.getText());
            } catch (NumberFormatException e) {
                informacoesValidacao += "Valor de hora de trabalho inválido\n";
            }
        }

        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
