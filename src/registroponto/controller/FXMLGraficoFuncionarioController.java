package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.FuncionarioDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;

public class FXMLGraficoFuncionarioController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private BarChart<String, Double> barChart;
    @FXML
    private NumberAxis numberAxis;
    @FXML
    private CategoryAxis categoryAxis;
    @FXML
    private Button buttonVoltar;

    private final ObservableList<String> observableListMeses = FXCollections.observableArrayList();

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final FuncionarioDAO funcionarioDAO = new FuncionarioDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        funcionarioDAO.setConnection(connection);
        carregarGrafico();
    }

    public void carregarGrafico() {
        String[] arrayMeses = {"Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"};
        observableListMeses.addAll(Arrays.asList(arrayMeses));

        categoryAxis.setCategories(observableListMeses);

        Map<String, ArrayList> dados = funcionarioDAO.buscarQuantidadeHorasTrabalhadas(FXMLGraficosController.funcionarioGrafico);

        dados.entrySet().stream().map((dadosItem) -> {
            XYChart.Series<String, Double> series = new XYChart.Series<>();
            series.setName(dadosItem.getKey());
            for (int i = 0; i < dadosItem.getValue().size(); i = i + 2) {
                String mes;
                Double horasTrabalhadas;

                mes = retornaNomeMes((int) dadosItem.getValue().get(i));
                horasTrabalhadas = (Double) dadosItem.getValue().get(i + 1);

                series.getData().add(new XYChart.Data<>(mes, horasTrabalhadas));
            }
            return series;
        }).forEachOrdered((series) -> {
            barChart.getData().add(series);
        });
    }

    @FXML
    private void handleButtonVoltar(ActionEvent event) {
        try {
            AnchorPane tela = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLGraficos.fxml"));
            anchorPane.getChildren().setAll(tela);
        } catch (IOException ex) {
            Logger.getLogger(FXMLGraficoFuncionarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String retornaNomeMes(int mes) {
        switch (mes) {
            case 1:
                return "Jan";
            case 2:
                return "Fev";
            case 3:
                return "Mar";
            case 4:
                return "Abr";
            case 5:
                return "Mai";
            case 6:
                return "Jun";
            case 7:
                return "Jul";
            case 8:
                return "Ago";
            case 9:
                return "Set";
            case 10:
                return "Out";
            case 11:
                return "Nov";
            case 12:
                return "Dez";
            default:
                return "";
        }
    }
}
