package registroponto.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import registroponto.model.dao.SetorDAO;
import registroponto.model.database.Database;
import registroponto.model.database.DatabaseFactory;
import registroponto.model.domain.Setor;

public class FXMLCadastroSetorAlterarController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ComboBox<Setor> comboBoxSetores;
    @FXML
    private TextField textFieldNome;
    @FXML
    private Button buttonCancelar;
    @FXML
    private Button buttonConfirmar;

    private List<Setor> listSetores;
    private ObservableList<Setor> observableListSetores;

    private final Database database = DatabaseFactory.getDatabase("postgresql");
    private final Connection connection = database.conectar();
    private final SetorDAO setorDAO = new SetorDAO();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setorDAO.setConnection(connection);

        carregarComboBoxSetores();

        comboBoxSetores.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> selecionarItemListViewSetores(newValue));
    }

    public void carregarComboBoxSetores() {
        listSetores = setorDAO.listar();
        observableListSetores = FXCollections.observableArrayList(listSetores);
        comboBoxSetores.setItems(observableListSetores);
    }

    public void selecionarItemListViewSetores(Setor setor) {
        textFieldNome.setText(setor.getNome());
    }

    @FXML
    public void handleButtonConfirmar() {
        if (validarEntradaDados()) {
            Setor setor = comboBoxSetores.getValue();
            setor.setNome(textFieldNome.getText());

            if (setorDAO.alterar(setor)) {
                alertSucesso();
            } else {
                alertErro();
            }

            voltarTela();
        }
    }

    @FXML
    public void handleButtonCancelar() {
        voltarTela();
    }

    public void voltarTela() {
        try {
            AnchorPane a = FXMLLoader.load(getClass().getResource("/registroponto/view/FXMLCadastros.fxml"));
            anchorPane.getChildren().setAll(a);
        } catch (IOException ex) {
            Logger.getLogger(FXMLCadastroSetorInserirController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void alertSucesso() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText("Alteração de Setor");
        alert.setContentText("Setor alterado com sucesso!");
        alert.show();
    }

    public void alertErro() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("Alteração de Setor");
        alert.setContentText("Não foi possível alterar o setor!");
        alert.show();
    }

    public boolean validarEntradaDados() {
        String informacoesValidacao = "";

        if (comboBoxSetores.getSelectionModel().getSelectedItem() == null) {
            informacoesValidacao += "Selecione um setor para remoção\n";
        }
        if (textFieldNome.getText().equals("")) {
            informacoesValidacao += "Informe o nome\n";
        }

        if (informacoesValidacao.length() == 0) {
            return true;
        }
        alertValidação(informacoesValidacao);
        return false;
    }

    public void alertValidação(String dadosInvalidos) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Existem campos com valores inválidos!");
        alert.setContentText(dadosInvalidos);
        alert.show();
    }
}
