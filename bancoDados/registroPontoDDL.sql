CREATE TABLE setor (
	cod_setor		SERIAL			NOT NULL,
	nome			VARCHAR(20)		NOT NULL,

	CONSTRAINT pk_setor
		PRIMARY KEY (cod_setor)
);

CREATE TABLE cargo (
	cod_cargo				SERIAL			NOT NULL,
	nome 					VARCHAR(30)		NOT NULL,
	setor					INTEGER			NOT NULL,
	valor_hora_trabalho		NUMERIC(7,2)	NOT NULL,
	nivel_sistema			INTEGER			NOT NULL,

	CONSTRAINT pk_cargo
		PRIMARY KEY (cod_cargo),

	CONSTRAINT fk_cargo_setor
		FOREIGN KEY (setor)
		REFERENCES setor(cod_setor)
);

CREATE TABLE funcionario (
	cod_funcionario			SERIAL			NOT NULL,
	cargo					INTEGER			NOT NULL,
	nome					VARCHAR(50)		NOT NULL,
	cpf						CHAR(11)		NOT NULL,
	senha					VARCHAR(20)		NOT NULL,

	CONSTRAINT pk_funcionario
		PRIMARY KEY (cod_funcionario),

	CONSTRAINT fk_funcionario_cargo
		FOREIGN KEY (cargo)
		REFERENCES cargo(cod_cargo)
);

CREATE TABLE periodo (
	cod_mes					INTEGER			NOT NULL,
	cod_ano					CHAR(4)			NOT NULL,
	label					VARCHAR(7)		NOT NULL,

	CONSTRAINT pk_periodo
		PRIMARY KEY (cod_mes, cod_ano)
);

CREATE TABLE ponto (
	cod_ponto				SERIAL			NOT NULL,
	ano						CHAR(4)			NOT NULL,
	mes						INTEGER			NOT NULL,
	funcionario				INTEGER			NOT NULL,
	hora_inicio				TIME			NOT NULL,
	hora_fim				TIME			NULL,

	CONSTRAINT pk_ponto
		PRIMARY KEY (cod_ponto),

	CONSTRAINT fk_ponto_funcionario
		FOREIGN KEY (funcionario)
		REFERENCES funcionario(cod_funcionario),

	CONSTRAINT fk_ponto_periodo
		FOREIGN KEY (mes, ano)
		REFERENCES periodo(cod_mes, cod_ano)
);

CREATE TABLE horario_periodo (
	funcionario				INTEGER			NOT NULL,
	ano						CHAR(4)			NOT NULL,
	mes						INTEGER			NOT NULL,
	carga_horaria			NUMERIC(7,2)	NOT NULL,

	CONSTRAINT pk_horario_periodo
		PRIMARY KEY (funcionario, ano, mes),

	CONSTRAINT pk_horario_periodo_periodo
		FOREIGN KEY (mes, ano)
		REFERENCES periodo(cod_mes, cod_ano),

	CONSTRAINT pk_horario_periodo_funcionario
		FOREIGN KEY (funcionario)
		REFERENCES funcionario(cod_funcionario)
);


INSERT INTO setor(nome)
	VALUES ('Financeiro');

INSERT INTO setor(nome)
	VALUES ('Operacional');


INSERT INTO cargo(nome, setor, valor_hora_trabalho, nivel_sistema)
	VALUES ('Diretor Geral', 1, 100.00, 3);

INSERT INTO cargo(nome, setor, valor_hora_trabalho, nivel_sistema)
	VALUES ('Gerente Operacional', 2, 15.00, 2);

INSERT INTO cargo(nome, setor, valor_hora_trabalho, nivel_sistema)
	VALUES ('Empacotador', 2, 10.00, 1);


INSERT INTO funcionario(cargo, nome, cpf, senha)
	VALUES (1, 'Herik', '11112222333', '123');

INSERT INTO funcionario(cargo, nome, cpf, senha)
	VALUES (2, 'Pedro', '44445555666', '456');

INSERT INTO funcionario(cargo, nome, cpf, senha)
	VALUES (3, 'Jorge', '66667777888', '678');


INSERT INTO periodo(cod_mes, cod_ano, label)
	VALUES (5, '2019', '2019/5');

INSERT INTO periodo(cod_mes, cod_ano, label)
	VALUES (6, '2019', '2019/6');


INSERT INTO ponto(ano, mes, funcionario, hora_inicio, hora_fim)
	VALUES ('2019', 5, 1, '07:00:00', '09:00:00');

INSERT INTO ponto(ano, mes, funcionario, hora_inicio, hora_fim)
	VALUES ('2019', 5, 2, '08:00:00', '10:00:00');

INSERT INTO ponto(ano, mes, funcionario, hora_inicio, hora_fim)
	VALUES ('2019', 5, 3, '09:00:00', '11:00:00');


INSERT INTO horario_periodo(funcionario, ano, mes, carga_horaria)
	VALUES (1, '2019', 5, 02.00);

INSERT INTO horario_periodo(funcionario, ano, mes, carga_horaria)
	VALUES (2, '2019', 5, 02.00);

INSERT INTO horario_periodo(funcionario, ano, mes, carga_horaria)
	VALUES (3, '2019', 5, 02.00);